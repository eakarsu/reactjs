import React, {Component, PropTypes} from 'react';
import {reduxForm} from 'redux-form';
import {addContact} from '../actions/index';
import {Link} from 'react-router';

class ContactNew extends Component {

    /*static contextTypes = {
        router: PropTypes.object
    };
*/
    onSubmit(props) {
        this.props.addContact(props)
            .then(() => {
                //create new contact and go to root menu
                this.context.router.push('/');
            });
    }


    render() {
        const {fields: {firstname, lastname, title, notes, phone, email, city, state, country}, handleSubmit} = this.props;

        return (
            <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                <h3>Create A New Contact</h3>
                <Link to="/">Back To Contacts List</Link>

                <div className={`input-group ${firstname.touched && firstname.invalid ? 'has-danger' : ''}`}>
                    <span className="input-group-addon" id="basic-addon3">First Name:</span>
                    <input type="text" className="form-control" {...firstname}
                           aria-describedby="basic-addon3"/>
                    <div className="text-help">
                        {firstname.touched ? firstname.error : ''}
                    </div>
                </div>
                <div className={`input-group ${lastname.touched && lastname.invalid ? 'has-danger' : ''}`}>
                    <span className="input-group-addon" id="basic-addon3">Last Name:</span>
                    <input type="text" className="form-control" {...lastname}
                           aria-describedby="basic-addon3"/>
                    <div className="text-help">
                        {lastname.touched ? lastname.error : ''}
                    </div>
                </div>
                <div className="input-group">
                    <span className="input-group-addon" id="basic-addon3">Title:</span>
                    <input type="text" className="form-control" aria-describedby="basic-addon3" {...title}/>
                </div>
                <div className="input-group">
                    <span className="input-group-addon" id="basic-addon3">Notes:</span>
                    <input type="text" className="form-control" aria-describedby="basic-addon3" {...notes}/>
                </div>
                <div className="input-group">
                    <span className="input-group-addon" id="basic-addon3">Phone:</span>
                    <input type="text" className="form-control" aria-describedby="basic-addon3" {...phone}/>
                </div>
                <div className={`input-group ${email.touched && email.invalid ? 'has-danger' : ''}`}>
                    <span className="input-group-addon" id="basic-addon3">Email:</span>
                    <input type="text" className="form-control" aria-describedby="basic-addon3" {...email}/>
                    <div className="text-help">
                        {email.touched ? email.error : ''}
                    </div>
                </div>
                <div className="input-group">
                    <span className="input-group-addon" id="basic-addon3">City:</span>
                    <input type="text" className="form-control" aria-describedby="basic-addon3" {...city}/>
                </div>
                <div className="input-group">
                    <span className="input-group-addon" id="basic-addon3">State:</span>
                    <input type="text" className="form-control" aria-describedby="basic-addon3" {...state}/>
                </div>
                <div className="input-group">
                    <span className="input-group-addon" id="basic-addon3">Country:</span>
                    <input type="text" className="form-control" {...country}
                           aria-describedby="basic-addon3"/>
                </div>

                <button type="submit" className="btn btn-primary">Submit</button>
                <Link to="/" className="btn btn-danger">Cancel</Link>

            </form>
        );

    }
}

ContactNew.contextTypes = {
    router: PropTypes.object
};

const validate = values => {
    const errors = {};

    if (!values.email) {
        errors.email = 'Enter your email'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'Invalid email address';
    }
    if (!values.firstname) {
        errors.firstname = 'Enter First Name';
    }

    if (!values.lastname) {
        errors.lastname = 'Enter Last Name';
    }
    return errors;
}

export default reduxForm({
    form: 'ContactNewForm',
    fields: ['firstname', 'lastname', 'title', 'notes', 'phone', 'email', 'city', 'state', 'country'],
    validate
}, null, {addContact})(ContactNew);



