/**
 * Created by eakarsu on 6/5/16.
 */
import React, {Component, PropTypes} from 'react';
import {reduxForm} from 'redux-form';
import {deleteContact, editContact, updateContact} from '../actions/index';
import {Link} from 'react-router';
import ManageContacts from './manage_contacts';

class ContactUpdate extends Component {

    /*static contextTypes = {
        router: PropTypes.object
    };
*/
    componentWillMount() {
        this.props.editContact(this.props.params.email);
    }

    constructor() {
        super();
    }

    onDeleteClick(event, data) {
        event.preventDefault();
        this.props.deleteContact(this.props.params.email)
            .then(() => {
                this.context.router.push('/');
            });
    }

    //For update action, send only dirty (modified) fields with mandatory email field
    filterProps() {
        let sentProps = {};
        const fields = this.props.fields;
        sentProps["email"] = fields.email.value;
        for (var key in fields) {
            if (fields[key].dirty) {
                sentProps[key] = fields[key].value;
            }
        }
        return sentProps;
    }

    onUpdateClick(event, data) {
        event.preventDefault();
        this.props.updateContact(this.filterProps())
            .then(() => {
                // contact  has been created, navigate the user to the index
                // We navigate by calling this.context.router.push with the
                // new path to navigate to.
                this.context.router.push('/');
            });
    }

    render() {
        const contact = this.props.values;
        if (!contact) {
            return <div>Loading...</div>;
        }

        const {fields: {firstname, lastname, title, notes, phone, email, city, state, country}, handleSubmit} = this.props;

        return (

            <form >
                <h3>Edit Contact</h3>

                <div className="btn-toolbar">
                    <Link to="/">Back To Contacts List</Link>

                    <button
                        className="btn btn-primary"
                        onClick={this.onUpdateClick.bind(this)}>
                        Update
                    </button>
                    <Link to="/" className="btn btn-danger">Cancel</Link>
                    <button className="btn btn-danger"
                            onClick={this.onDeleteClick.bind(this)}>
                        Delete
                    </button>
                </div>

                <div className={`input-group ${firstname.touched && firstname.invalid ? 'has-danger' : ''}`}>
                    <span className="input-group-addon"
                          id="basic-addon3">First Name:</span>
                    <input type="text" className="form-control" {...firstname}
                           aria-describedby="basic-addon3"/>
                    <div className="text-help">
                        {firstname.touched ? firstname.error : ''}
                    </div>
                </div>
                <div className={`input-group ${lastname.touched && lastname.invalid ? 'has-danger' : ''}`}>
                    <span className="input-group-addon" id="basic-addon3">Last Name:</span>
                    <input type="text" className="form-control" {...lastname}
                           aria-describedby="basic-addon3"/>
                    <div className="text-help">
                        {lastname.touched ? lastname.error : ''}
                    </div>
                </div>
                <div className="input-group">
                    <span className="input-group-addon" id="basic-addon3">Title:</span>
                    <input type="text" className="form-control" aria-describedby="basic-addon3" {...title}/>
                </div>
                <div className="input-group">
                    <span className="input-group-addon" id="basic-addon3">Notes:</span>
                    <input type="text" className="form-control" aria-describedby="basic-addon3" {...notes}/>
                </div>
                <div className="input-group">
                    <span className="input-group-addon" id="basic-addon3">Phone:</span>
                    <input type="text" className="form-control" aria-describedby="basic-addon3" {...phone}/>
                </div>

                <div className={`input-group ${email.touched && email.invalid ? 'has-danger' : ''}`}>
                    <span className="input-group-addon" id="basic-addon3">Email:</span>
                    <input type="text" className="form-control" aria-describedby="basic-addon3" {...email}/>
                    <div className="text-help">
                        {email.touched ? email.error : ''}
                    </div>
                </div>

                <div className="input-group">
                    <span className="input-group-addon" id="basic-addon3">City:</span>
                    <input type="text" className="form-control" aria-describedby="basic-addon3" {...city}/>
                </div>
                <div className="input-group">
                    <span className="input-group-addon" id="basic-addon3">State:</span>
                    <input type="text" className="form-control" aria-describedby="basic-addon3" {...state}/>
                </div>
                <div className="input-group">
                    <span className="input-group-addon" id="basic-addon3">Country:</span>
                    <input type="text" className="form-control" {...country}
                           aria-describedby="basic-addon3"/>
                </div>

                <ManageContacts contactRecord={this.props.contactRecord}/>

            </form>

        );

    }


}

const validate = values => {
    const errors = {};

    if (!values.email) {
        errors.email = 'Enter your email'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'Invalid email address';
    }
    if (!values.firstname) {
        errors.firstname = 'Enter First Name';
    }

    if (!values.lastname) {
        errors.lastname = 'Enter Last Name';
    }
    return errors;
}

ContactUpdate.contextTypes = {
    router: PropTypes.object
};

ContactUpdate.propTypes = {
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    load: PropTypes.func.isRequired,
    submitting: PropTypes.bool.isRequired
}

export default reduxForm({
        form: 'initializing',
        fields: ['firstname', 'lastname', 'title', 'notes', 'phone', 'email', 'city', 'state', 'country'],
        validate
    },
    state => ({ // mapStateToProps
        initialValues: state.contacts.contact,
        contactRecord: state.contacts.contact// will pull state into form's initialValues
    }),
    {load: updateContact, editContact, deleteContact, updateContact}      // mapDispatchToProps (will bind action creator to dispatch)
)(ContactUpdate)



