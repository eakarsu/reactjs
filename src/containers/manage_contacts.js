import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {deleteRelatedContact, addRelatedContact, editContact} from '../actions/index';

class ManageContacts extends Component {

    /*static contextTypes = {
        router: PropTypes.object
    };
    */
    onRelatedContactDeleteClick(props) {
        event.preventDefault();
        this.props.deleteRelatedContact(props.params.personemail, props.params.contactemail)
            .then(() => {
                this.context.router.push("/update/" + props.params.personemail);
            });
    }

    onRelatedContactAddClick(event, data) {
        event.preventDefault();

        const contactemail = this.refs.selectedContact.value;
        const personemail = this.props.contactRecord.email;
        const contactObject = this.props.contactRecord.potentialcontacts.find((rec) => rec.email == contactemail);
        const fullname = `${contactObject.firstname}  ${contactObject.lastname}`;
        this.props.addRelatedContact(personemail, contactemail, fullname)
            .then(() => {
                this.context.router.push("/update/" + personemail);
            });
    }

    onGetContact(props) {


        const contactemail = props.contactemail;
        this.props.editContact(contactemail)
            .then(() => {
                this.context.router.push("/update/" + contactemail);
            });
    }

    handleContact(contacts, onRelatedContactDeleteClick, props) {
        let onGetContact = this.onGetContact.bind(this)
        return (
            contacts.relatedcontacts.map(function (nextcontact) {

                return (
                    <li className="list-group-item" key={nextcontact.contactemail}>
                        <div className="btn-group" role="group">
                            <button type="button"
                                    className="btn btn-default"
                                    onClick={() => onRelatedContactDeleteClick(
                                                            {   ...props,
                                                                params:{personemail:nextcontact.email,
                                                                contactemail:nextcontact.contactemail}})
                                                            }
                            >
                                Delete
                            </button>

                            <a key={nextcontact.contactemail}
                               href="#"
                               onClick={() => onGetContact(
                                                            {   ...props,
                                                                contactemail:nextcontact.contactemail})
                                                            }
                            >
                                {nextcontact.contactfullname}
                            </a>

                            <div className="help-block"></div>
                        </div>
                    </li>
                )
            })
        );
    }

    renderContactEmails(contacts) {
        return (
            <ul className="list-group">
                <li className="list-group-item" key={this.props.email}>
                    <select ref="selectedContact"
                    >
                        <option value="">Select a Contact</option>
                        {contacts.potentialcontacts.map(contOption =>
                            <option value={contOption.email}
                                    key={contOption.email}
                            >
                                {contOption.firstname} {contOption.lastname}
                            </option>)}
                    </select>
                    <button
                        className="btn btn-default"
                        onClick={this.onRelatedContactAddClick.bind(this)}>
                        Add Contact
                    </button>

                    <div className="help-block"></div>
                </li>
                {this.handleContact(contacts, this.onRelatedContactDeleteClick.bind(this), this.props.contactRecord)}
            </ul>
        );
    }

    render() {

        if (!this.props.contactRecord || Object.keys(this.props.contactRecord) == 0) {
            return <div>Loading...</div>;
        }


        return this.renderContactEmails(this.props.contactRecord)


    }
}

ManageContacts.contextTypes = {
    router: PropTypes.object
};

const mapStateToProps = (state) => {
    return {
        contacts: state.contacts.all,
        contactRecord: state.contacts.contact
    };
}
export default connect(mapStateToProps, {deleteRelatedContact, addRelatedContact, editContact})(ManageContacts);
