import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router';
import SearchBar from './search_bar';

class ContactList extends Component {

    /*static contextTypes = {
        router: PropTypes.object
    };
*/

    renderContact(contact) {
        return (
            <tr key={contact.email}>
                <td>
                    <Link to={"/update/" + contact.email}>
                        <button
                            className="btn btn-default">
                            Edit
                        </button>
                    </Link>
                </td>


                <td>{contact.firstname}</td>
                <td>{contact.lastname}</td>
                <td>{contact.title}</td>
                <td>{contact.notes}</td>
                <td>{contact.phone}</td>
                <td>{contact.email}</td>
                <td>{contact.city}</td>
                <td>{contact.state}</td>
                <td>{contact.country}</td>

            </tr>
        );
    }


    render() {

        return (
            <div>
                <div className="text-xs-right">
                    <Link to="contacts/new" className="btn btn-primary">
                        Add a Contact
                    </Link>
                </div>
                <SearchBar />
                <table className="table table-hover">
                    <thead>
                    <tr>
                        <th/>
                        <th/>
                        <th>Firs Name</th>
                        <th>Last Name</th>
                        <th>Title</th>
                        <th>Notes</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>City</th>
                        <th>State</th>
                        <th>Country</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.props.contacts.map(this.renderContact)}
                    </tbody>
                </table>
            </div>
        );
    }
}

ContactList.contextTypes = {
    router: PropTypes.object
};

const mapStateToProps = (state) => {
    return {contacts: state.contacts.all};
}

export default connect(mapStateToProps)(ContactList);
