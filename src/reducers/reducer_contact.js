import {
    FETCH_CONTACTS,
    EDIT_CONTACT,
    DELETE_CONTACT,
    ADD_CONTACT,
    DELETE_ASSOCIATED_CONTACT,
    ADD_ASSOCIATED_CONTACT
} from '../actions/index';

const INITIAL_STATE = {all: [], contact: null};

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case FETCH_CONTACTS:
        {
            return {...state, all: action.payload.data};
        }
        case EDIT_CONTACT:
        {
            let newState = {...state};
            if (action.payload.data)
                newState = {...state, contact: action.payload.data[0]};
            return newState;
        }
        case DELETE_ASSOCIATED_CONTACT:
        {

            let newState = {...state, contact: action.payload.data[0]};
            return newState;
        }
        case ADD_ASSOCIATED_CONTACT:
        {

            let newState = {...state, contact: action.payload.data[0]};
            return newState;
        }
        case DELETE_CONTACT:
        {
            //Remove deleted contact in contact list
            let contact = state.contact;
            let contacts = state.all.filter(function (c) {
                return c.email != contact.email;
            });

            let newState = {...state, all: contacts};
            return newState;
        }
        case ADD_CONTACT:
        {
            //add new contact in contact list
            let contact = action.payload.data;
            state.all.push(contact);
            let newState = {...state};

            return newState;
        }
        default:
            return state;

    }
}
