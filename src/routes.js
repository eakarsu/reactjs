/**
 * Created by eakarsu on 6/5/16.
 */
import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './components/app';
import ContactsIndex from './containers/contact_list';
import ContactNew from './containers/contact_new';
import ContactUpdate from './containers/contact_update';
import ManageContacts from './containers/manage_contacts';

export default (
    <Route path="/" component={App}>
        <IndexRoute component={ContactsIndex} />
        <Route path="/contacts/new" component={ContactNew} />
        <Route path="/update/:email" component={ContactUpdate} />
    </Route>
);