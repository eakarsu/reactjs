import axios from 'axios';


const ROOT_URL = `http://localhost:3000`;

export const FETCH_CONTACTS = 'FETCH_CONTACTS';
export const EDIT_CONTACT = 'EDIT_CONTACT';
export const DELETE_CONTACT = 'DELETE_CONTACT';
export const ADD_CONTACT = 'ADD_CONTACT';
export const DELETE_ASSOCIATED_CONTACT = 'DELETE_ASSOCIATED_CONTACT';
export const ADD_ASSOCIATED_CONTACT = 'ADD_ASSOCIATED_CONTACT';

export function fetchContact(queryword) {

    const url = `${ROOT_URL}/contacts/search/${queryword}`;
    const request = axios.get(url);

    return {
        type: FETCH_CONTACTS,
        payload: request
    };
}

export function editContact(email) {

    const url = `${ROOT_URL}/contacts/edit/${email}`;
    const request = axios.get(url);
    return {
        type: EDIT_CONTACT,
        payload: request
    };
}
export function deleteContact(email) {

    const url = `${ROOT_URL}/contacts/delete/${email}`;
    const request = axios.delete(url);

    return {
        type: DELETE_CONTACT,
        payload: request
    };
}

export function addContact(props) {

    const url = `${ROOT_URL}/contacts/add`;
    const request = axios.post(url, props);

    return {
        type: ADD_CONTACT,
        payload: request
    };
}
export function updateContact(props) {

    const url = `${ROOT_URL}/contacts/update`;
    const request = axios.post(url, props);

    return {
        type: ADD_CONTACT,
        payload: request
    };
}

export function deleteRelatedContact(personemail, contactemail) {

    const url = `${ROOT_URL}/contacts/deleteAssociated/${personemail}/${contactemail}`;
    const request = axios.delete(url);

    return {
        type: DELETE_ASSOCIATED_CONTACT,
        payload: request
    };
}

export function addRelatedContact(personemail, contactemail, fullname) {

    const url = `${ROOT_URL}/contacts/addAssociated/${personemail}/${contactemail}/${fullname}`;
    const request = axios.put(url);

    return {
        type: ADD_ASSOCIATED_CONTACT,
        payload: request
    };
}