import { renderComponent, expect } from '../test_helper';
import SearchBar from '../../src/containers/search_bar';

describe('SearchBar', () => {
    let searchBarComponent;

    beforeEach(() => {
        searchBarComponent = renderComponent(SearchBar);
     });

    it('has the correct class', () => {
        expect(searchBarComponent).to.have.class('input-group');
    });

    it('has a input', () => {
        expect(searchBarComponent.find('input[class="form-control"]')).to.exist;
    });

    it('has a button', () => {
        expect(searchBarComponent.find('button')).to.exist;
    });

    describe('Clicking search button with search input', () => {
        beforeEach(() => {
            const node = searchBarComponent.find('input');
            node.val('e');
            node.simulate("change");
            node.simulate("keyDown", {key: "Enter", keyCode: 13, which: 13});
        });

        it('when submitted, clears the input', () => {
            const html = searchBarComponent.find('input[class="form-control"]');
            expect(searchBarComponent.find('input[class="form-control"]')).to.have.attr('value', '');
        });
    });

});