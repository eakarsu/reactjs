import {renderComponent, expect} from '../test_helper';
import ContactList from '../../src/containers/contact_list';

describe('Contact List', () => {
    let contactListComponent;

    beforeEach(() => {
        contactListComponent = renderComponent(ContactList);
    });

    it('has div element with the correct class', () => {
        expect(contactListComponent.find('div[class="text-xs-right"]')).to.exist;
    });

    it('has table element with the correct class', () => {
        expect(contactListComponent.find('table')).class('table');
    });

    it('has a input', () => {
        expect(contactListComponent.find('input[class="form-control"]')).to.exist;
    });

    it('has a button', () => {
        expect(contactListComponent.find('button')).to.exist;
    });

    describe('Clicking search button with search input1', () => {
        beforeEach(() => {
            const node = contactListComponent.find('input');
            node.val('e');
            node.simulate("change");
            node.simulate("keyDown", {key: "Enter", keyCode: 13, which: 13});
        });

        it('when submitted, clears the input', () => {
            expect(contactListComponent.find('input[class="form-control"]')).to.have.attr('value', '');
        });
    });

    describe('Clicking search button with search input2', () => {
        beforeEach(() => {
            contactListComponent = renderComponent(ContactList, null, {
                contacts: {
                    all: [{
                        lastname: "Akarsu",
                        firstname: "Erol",
                        email: "eakarsu@gmail.com"
                    }, {lastname: "Akarsu", firstname: "Ese", email: "ese@gmail.com"}]
                }
            });
        });

        it("Check 2 table elems populated", () => {
            expect(contactListComponent.find('tr').length).to.equal(3);
        });

        it("Check first table elem individually", () => {
            const firstRow = contactListComponent.find('tr').eq(1);
            const firstNameTd = firstRow.find("td").eq(1);
            const lastNameTd = firstRow.find("td").eq(2);
            const emailTd = firstRow.find("td").eq(6);
            expect(firstNameTd.text()).to.equal("Erol");
            expect(lastNameTd.text()).to.equal("Akarsu");
            expect(emailTd.text()).to.equal("eakarsu@gmail.com");
        });

        it("Check second table elem individually", () => {
            const firstRow = contactListComponent.find('tr').eq(2);
            const firstNameTd = firstRow.find("td").eq(1);
            const lastNameTd = firstRow.find("td").eq(2);
            const emailTd = firstRow.find("td").eq(6);
            expect(firstNameTd.text()).to.equal("Ese");
            expect(lastNameTd.text()).to.equal("Akarsu");
            expect(emailTd.text()).to.equal("ese@gmail.com");
        });

        it('has a button with value Edit ', () => {
            expect(contactListComponent.find('button').eq(1)).to.have.text('Edit');
        });

    });

});