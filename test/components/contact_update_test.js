import {renderComponent, expect} from '../test_helper';
import ContactUpdate from '../../src/containers/contact_update';

describe('Contact Update/Edit', () => {
    let contactUpdateComponent;

    beforeEach(() => {
        contactUpdateComponent = renderComponent(ContactUpdate, {params: {email: "eakarsu@gmail.com"}},
            {
                contacts: {
                    contact: {
                        firstname: "Erol", lastname: "Akarsu", email: "eakarsu@gmail.com",
                        potentialcontacts: [{
                            "firstname": "Nahit",
                            "lastname": "Akarsu",
                            "email": "nakarsu@gmail.com",
                        },
                            {
                                "firstname": "Selman",
                                "lastname": "Akarsu",
                                "email": "sakarsu@gmail.com",
                            }],
                        relatedcontacts: [{
                            "email": "eakarsu@gmail.com",
                            "contactemail": "ese@gmail.com",
                            "contactfullname": "Ese Akarsu"
                        },
                            {
                                "email": "eakarsu@gmail.com",
                                "contactemail": "macit@gmail.com",
                                "contactfullname": "Macit Akarsu"
                            }]
                    }
                }
            });
    });

    it('has <a> element with text Back To Contacts List', () => {
        expect(contactUpdateComponent.find('a').eq(0)).to.have.text('Back To Contacts List');
    });

    it('has <a> element with text Cancel', () => {
        expect(contactUpdateComponent.find('a').eq(1)).to.have.text('Cancel');
    });

    it('has update button', () => {
        expect(contactUpdateComponent.find('button').eq(0)).to.have.text('Update');
    });

    it('has delete button', () => {
        expect(contactUpdateComponent.find('button').eq(1)).to.have.text('Delete');
    });

    it('It has input element for first name', () => {
        expect(contactUpdateComponent.find('input').eq(0)).to.have.attr('name', 'firstname');
    });

    it('It has input element for last name', () => {
        expect(contactUpdateComponent.find('input').eq(1)).to.have.attr('name', 'lastname');
    });

    it('select element has 3  potential contacts', () => {
        expect(contactUpdateComponent.find('option').length).eql(3);
    });

    it('first opton for select element has valid value', () => {
        expect(contactUpdateComponent.find('option').eq(1)).to.have.attr('value', 'nakarsu@gmail.com');
    });
    it('second opton for select element has valid value', () => {
        expect(contactUpdateComponent.find('option').eq(2)).to.have.attr('value', 'sakarsu@gmail.com');
    });

    it('first associated contact has valid value', () => {
        expect(contactUpdateComponent.find('a[href="#"]').eq(0)).to.have.text('Ese Akarsu');
    });

    it('second associated contact has valid value', () => {
        expect(contactUpdateComponent.find('a[href="#"]').eq(1)).to.have.text('Macit Akarsu');
    });
});


