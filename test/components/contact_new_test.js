import {renderComponent, expect} from '../test_helper';
import ContactNew from '../../src/containers/contact_new';

describe('Contact List', () => {
    let contactNewComponent;

    beforeEach(() => {
        contactNewComponent = renderComponent(ContactNew);
    });

    it('has <a> element with text Back To Contacts List', () => {
        expect(contactNewComponent.find('a').eq(0)).to.have.text('Back To Contacts List');
    });

    it('has <a> element with text Cancel', () => {
        expect(contactNewComponent.find('a').eq(1)).to.have.text('Cancel');
    });

    it('has submit button', () => {
        expect(contactNewComponent.find('button')).to.have.text('Submit');
    });

    it('It has input element for first name', () => {
        expect(contactNewComponent.find('input').eq(0)).to.have.attr('name', 'firstname');
    });

    it('It has input element for last name', () => {
        expect(contactNewComponent.find('input').eq(1)).to.have.attr('name', 'lastname');
    });

    it('It has input element for email', () => {
        expect(contactNewComponent.find('input').eq(5)).to.have.attr('name', 'email');
    });
});