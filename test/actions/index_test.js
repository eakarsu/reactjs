import {expect} from '../test_helper';
import {
    fetchContact,
    deleteContact,
    editContact,
    addContact,
    deleteRelatedContact,
    addRelatedContact
} from '../../src/actions/index';
import {
    FETCH_CONTACTS
} from '../../src/actions/index';

function promisedMap(array, transform) {
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            resolve(array.map(transform));
        }, 100);
    });
}

describe('actions', () => {

    it('eventually returns the results', function () {
        var input = [1, 2, 3];
        var transform = function (x) {
            return x * 2;
        };

        return promisedMap(input, transform).then(function (result) {
            
            expect(result).to.eql([2, 4, 6]);
        });
    });

    it('Fetch contacts has correct returned type', () => {
        const action = fetchContact('e');
        expect(action.type).to.eql(FETCH_CONTACTS);
    });

    it('Fetch contacts got 3 persons returned', () => {
        const action = fetchContact('e');
        return action.payload.then((result) => {
                
                const data = result.data;
                expect(data[0].email).to.eql("ese@gmail.com");
                expect(data[1].email).to.eql("selman077@gmail.com");
                expect(data[2].email).to.eql("eakarsu@gmail.com");
            }
        );
    });

    it('Edit contact is receiving correct data', () => {
        const action = editContact('eakarsu@gmail.com');
        return action.payload.then((result) => {
                
                const data = result.data;
                expect(data[0].email).to.eql("eakarsu@gmail.com");
                expect(data[0].firstname).to.eql("Erol");
                expect(data[0].lastname).to.eql("Akarsu");
            }
        );
    });

    it('Add new  contact', () => {
        const action = addContact({email: "eseakarsu@gmail.com", firstname: "Esexx", lastname: "Akarsuxx"});
        return action.payload.then((result) => {
                const data = result.data;
                
                expect(data.email).to.eql("eseakarsu@gmail.com");
                expect(data.firstname).to.eql("Esexx");
                expect(data.lastname).to.eql("Akarsuxx");
            }
        );
    });

    it('Add second new  contact', () => {
        const action = addContact({email: "eseakarsu2@gmail.com", firstname: "Esexx2", lastname: "Akarsuxx2"});
        return action.payload.then((result) => {
                const data = result.data;
                
                expect(data.email).to.eql("eseakarsu2@gmail.com");
                expect(data.firstname).to.eql("Esexx2");
                expect(data.lastname).to.eql("Akarsuxx2");
            }
        );
    });

    it('Add associated  contact', () => {
        const action = addRelatedContact("eseakarsu@gmail.com", "eseakarsu2@gmail.com", "Esexx Akarsuxx");
        return action.payload.then((result) => {
                const data = result.data[0];
                const relatedContacts = data.relatedcontacts;
                
                expect(data.email).to.eql("eseakarsu@gmail.com");
                const relContact = relatedContacts.find((c) => c.contactemail == "eseakarsu2@gmail.com");
                expect(relContact.contactemail).to.eql("eseakarsu2@gmail.com");
            }
        );
    });

    it('Add reverse associated  contact', () => {
        const action = addRelatedContact("eseakarsu2@gmail.com", "eseakarsu@gmail.com", "Esexx2 Akarsuxx2");
        return action.payload.then((result) => {
                const data = result.data[0];
                const relatedContacts = data.relatedcontacts;
                
                expect(data.email).to.eql("eseakarsu2@gmail.com");
                const relContact = relatedContacts.find((c) => c.contactemail == "eseakarsu@gmail.com");
                expect(relContact.contactemail).to.eql("eseakarsu@gmail.com");
            }
        );
    });

    it('Delete associated  contact', () => {
        const action = deleteRelatedContact("eseakarsu@gmail.com", "eseakarsu2@gmail.com");
        return action.payload.then((result) => {
                
                const data = result.data[0];
                const relatedContacts = data.relatedcontacts;
                expect(data.email).to.eql("eseakarsu@gmail.com");
                expect(data.firstname).to.eql("Esexx");
                expect(data.lastname).to.eql("Akarsuxx");
                const relContact = relatedContacts.find((c) => c.contactemail == "eseakarsu2@gmail.com");
                expect(relContact).to.eql(undefined);

            }
        );
    });


    it('Delete a  contact', () => {
        const action = deleteContact("eseakarsu@gmail.com");
        return action.payload.then((result) => {
                const data = result.data;
                
                expect(data).to.eql({result: "ok"});
            }
        );
    });

    it('Delete second  contact', () => {
        const action = deleteContact("eseakarsu2@gmail.com");
        return action.payload.then((result) => {
                const data = result.data;
                
                expect(data).to.eql({result: "ok"});
            }
        );
    });

});
