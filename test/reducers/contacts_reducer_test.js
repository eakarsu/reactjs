import {expect} from '../test_helper';
import contactReducer from '../../src/reducers/reducer_contact';
import {
    FETCH_CONTACTS,
    EDIT_CONTACT,
    DELETE_CONTACT,
    ADD_CONTACT,
    DELETE_ASSOCIATED_CONTACT,
    ADD_ASSOCIATED_CONTACT
} from '../../src/actions/index';

describe('Contact Reducer', () => {

    it('handles action with unknown type', () => {
        expect(contactReducer(undefined, {})).to.eql({all: [], contact: null});
    });

    it('handles action of type FETCH_CONTACTS', () => {
        const action = {
            type: FETCH_CONTACTS,
            payload: {
                data: [{
                    "email": "ese@gmail.com",
                    "firstname": "Ese",
                    "lastname": "Akarsu"
                }, {"email": "ese2@gmail.com", "firstname": "Ese2", "lastname": "Akarsu2"}]
            }
        };
        expect(contactReducer([], action)).to.eql({
            all: [{
                "email": "ese@gmail.com",
                "firstname": "Ese",
                "lastname": "Akarsu"
            }, {"email": "ese2@gmail.com", "firstname": "Ese2", "lastname": "Akarsu2"}]
        });
    });

    it('handles action of type EDIT_CONTACT', () => {
        const action = {
            type: EDIT_CONTACT,
            payload: {
                data: [{
                    "email": "ese@gmail.com",
                    "firstname": "Ese",
                    "lastname": "Akarsu"
                }, {"email": "ese2@gmail.com", "firstname": "Ese2", "lastname": "Akarsu2"}]
            }
        };
        expect(contactReducer([], action)).to.eql({
            contact: {
                "email": "ese@gmail.com",
                "firstname": "Ese",
                "lastname": "Akarsu"
            }
        });
    });

    it('handles action of type ADD_CONTACT', () => {
        const action = {
            type: ADD_CONTACT,
            payload: {data: {"email": "ese@gmail.com", "firstname": "Ese", "lastname": "Akarsu"}}
        };
        expect(contactReducer({all: [{"email": "ese@gmail.com"}]}, action)).to.eql({
            all: [{"email": "ese@gmail.com"}, {
                "email": "ese@gmail.com",
                "firstname": "Ese",
                "lastname": "Akarsu"
            }]
        });
    });

    it('handles action of type DELETE_CONTACT', () => {
        const action = {type: DELETE_CONTACT, payload: {}};
        expect(contactReducer({
            all: [{"email": "ese@gmail.com"}, {"email": "eakarsu@gmail.com"}],
            contact: {email: "ese@gmail.com"}
        }, action)).to.eql({all: [{"email": "eakarsu@gmail.com"}], contact: {email: "ese@gmail.com"}});
    });

    it('handles action of type ADD_ASSOCIATED_CONTACT', () => {
        const action = {
            type: ADD_ASSOCIATED_CONTACT,
            payload: {data: [{"email": "ese@gmail.com"}, {"email": "xyz@gmail.com"}]}
        };
        expect(contactReducer({all: [], contact: {email: "abc@gmail.com"}}, action)).to.eql({
            all: [],
            contact: {"email": "ese@gmail.com"}
        });
    });

    it('handles action of type DELETE_ASSOCIATED_CONTACT', () => {
        const action = {
            type: DELETE_ASSOCIATED_CONTACT,
            payload: {data: [{"email": "ese@gmail.com"}, {"email": "xyz@gmail.com"}]}
        };
        expect(contactReducer({all: [], contact: {email: "abc@gmail.com"}}, action)).to.eql({
            all: [],
            contact: {"email": "ese@gmail.com"}
        });
    });

});
